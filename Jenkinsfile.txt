pipeline {
    agent any
    stages {
        stage('Get playbook') {
            steps {
                git 'https://gitlab.com/jaya-narayana/devnetopscicdpipeline.git'
            }
        }
        stage('playbook Sanity test') {
            steps {
                sh 'ansible-lint -x 201,403 playbook_httpd.yml'
            }
        }
        stage('Ansible Run playbook') {
            steps {
                   'ansible-playbook playbook_httpd.yml'
            }
        }
    }
}
